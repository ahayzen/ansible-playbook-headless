#!/usr/bin/env python3
"""
Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
from configparser import ConfigParser
from logging import getLogger, DEBUG as logging_DEBUG
from os import listdir
from os.path import isfile, join
from systemd.journal import JournalHandler
from subprocess import Popen, PIPE, STDOUT
from time import sleep
from typing import List


class Logger(object):
    __logger = getLogger("container.update.systemd.log")
    debug = __logger.debug
    info = __logger.info
    warning = __logger.warning
    error = __logger.error
    critical = __logger.critical

    @classmethod
    def init(cls: object):
        cls.__logger.addHandler(JournalHandler())
        cls.__logger.setLevel(logging_DEBUG)


def run_command(cmd: str, args: List[str] = []):
    """ Run the given command and args """

    Logger.info("Executing command: %s with args: %s" % (cmd, args))

    # Run the command, catch any errors with the wrong filepath
    try:
        with Popen([cmd, *args], stdout=PIPE, stderr=STDOUT) as proc:
            # Stream the output of the command
            for line in proc.stdout:
                Logger.debug(line.decode("utf-8").strip())

            # If the command has a non zero exit code then we have failed
            if proc.wait() != 0:
                Logger.error("Failed to run command: %s with args: %s"
                             % (cmd, args))
                exit(proc.returncode)
    except FileNotFoundError as e:
        if e.strerror is not None:
            Logger.warning(e.strerror)

        Logger.error("Failed to run command: %s with args: %s" % (cmd, args))
        exit(e.errno)

    # Once command has completed wait for a period to allow
    # for the server load to reduce
    sleep(5)


class Container(object):
    def __init__(self, filepath: str):
        Logger.debug("Parsing file: %s" % filepath)

        config = ConfigParser()
        config.read(filepath)
        section = "update"

        # Mandatory parts
        self.directory = config.get(section, "directory")
        self.name = config.get(section, "name")
        self.service = config.get(section, "service")

        # Optional parts
        self.restart = config.getboolean(section, "restart", fallback=True)
        self.restart_group = config.get(section, "restartGroup", fallback=None)

    def build(self):
        """ Rebuild this container """

        Logger.info("Building container: %s with the directory: %s" %
                    (self.name, self.directory))

        run_command(
            "/usr/bin/docker",
            [
                "build", "--no-cache", "--pull",
                "--tag", self.name, self.directory,
            ]
        )


class Containers(object):
    def __init__(self, scan_dir: str):
        Logger.debug("Scanning directory for files: %s" % scan_dir)

        self.scan_files = [join(scan_dir, f) for f in listdir(scan_dir)
                           if isfile(join(scan_dir, f))]
        self.groups = {}
        self.ungrouped = []

    def prune(self):
        """ Prune any non used container data """

        Logger.info("Pruning containers")

        run_command("/usr/bin/docker", ["system", "prune", "--force"])

    def restart(self, services: List[str]):
        """ Restart all given services """

        if len(services) > 0:
            Logger.info("Restarting services: %s" % services)

            run_command("/bin/systemctl", ["restart", *services])

    def run(self):
        """ Run all of the definitions, groups first """

        def run_containers(containers):
            Logger.info("Processing: %s" % [c.name for c in containers])

            # Build each container
            list(map(lambda c: c.build(), containers))

            # Restart given containers
            self.restart([c.service for c in containers if c.restart])

            # Prune any extra content
            self.prune()

        Logger.info("Processing grouped containers")

        # Run the groups together
        list(map(lambda g: run_containers(g), self.groups.values()))

        Logger.info("Processing individual containers")

        # Run each ungrouped individually
        list(map(lambda c: run_containers([c]), self.ungrouped))

    def scan(self):
        """ Scan the directory for definitions """

        # Loop through files in folder
        for filepath in self.scan_files:
            # Load definition from file
            container = Container(filepath)

            # Add to ungrouped or groups
            if container.restart_group is None:
                self.ungrouped.append(container)
            else:
                self.groups[container.restart_group] = self.groups.get(
                    container.restart_group, []) + [container]


if __name__ == "__main__":
    Logger.init()

    containers = Containers("/opt/containers/conf.d")
    containers.scan()
    containers.run()
    exit(0)
