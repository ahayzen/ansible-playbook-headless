#!/bin/bash

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# If the container has already been built as an image
# then we don't call build. As the timer rebuild uses
# --no-cache so when this is run it would then rebuild.
#
# Instead we only build the image the first time and
# wait for the timer to rebuild any containers if there
# are changes.
if [ -z "$( /usr/bin/docker images -q "$1" )" ]; then
  /usr/bin/docker build --no-cache --tag="$1" "$2"
else
  echo "Container $1 has already been built, skipping"
fi
