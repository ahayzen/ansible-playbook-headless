This repository provides a headless collection of roles to be used in a playbook.
It is intended to be used in combination with other roles or collections - such as the `server` collection.

# Usage

Add collection to `requirements.yml` file

```
collections:
  - name: https://gitlab.com/ahayzen/ansible-playbook-headless
```

Then pull the collection for use

```
ansible-galaxy collection install -r requirements.yml --force
```

Then add the roles to your playbook

```
- name: playbook
  ...
  roles:
    - role: ahayzen.headless.role
```

## SSH key

SSH keys should be generated with the following command and the `id_rsa.pub`
should be set as the variable `admin_user_pub_key`.

```
ssh-keygen -t rsa -b 4096 -C "user@email.com" -f id_rsa
```

# Backup

The backup playbook will create a backup in the folder `/tmp/ansible-backup/<timestamp>/`.

# Restore

The restore playbook will use a backup from the folder `/tmp/ansible-backup/restore/`.

# Timers

The following timers are defined within the playbook. Note that all timers have `RandomizedDelaySec=60m` set.

Time | Name | Purpose
-----|------|--------
Monday 22:00 | `containers.weekly.timer` | Rebuild all containers and restart them
Monday 22:00 | `docker-compose-upgrade.timer` | Pull and restart change containers
Wednesday 22:00 | `apt-daily-upgrade.timer` | Unattended upgrades (with potential reboot)

# Container Users

The following container users are defined within the playbook. Note that these users are mapped into the subuid range of `unprivusr` (which is mapped to 900000).

## Users

Users uid's take the form `(9)1XXYY` where `XXYY` is the number of the first two letters of the container name.

Container | User | uid host | uid container | Groups
----------|------|----------|---------------|-------
EXAMPLE | `container-EXAMPLE` | `910524` | `10524` | `container-GROUP`

## Groups

Groups gid's take the form `(9)5XXYY`, where `XXYY` is the number of the first two letters of the group.

Group | gid host | gid container | Purpose
------|----------|---------------|--------
container-GROUP | `950718` | `50718` | example group
